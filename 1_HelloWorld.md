
## Premières étapes simples (issue, MR, ...)

Une fois connecté :
* Créer un nouveau projet simple aka "Blank project"
    * Il existe des projets templates fournis par défaut par Gitlab, vous pouvez également en créer facilement 👉 [🔗 Official documentation](https://docs.gitlab.com/ee/user/admin_area/custom_project_templates)
* Créer une première [issue](https://docs.gitlab.com/ee/user/project/issues/) "📃 Update README.md"
    * L'objectif de cette issue doit être d'ajouter un schéma [mermaid](https://mermaid.live)
* A partir de cette issue, créer une [Merge Request](https://docs.gitlab.com/ee/user/project/merge_requests/) et une branche pour cette issue
* En utilisant le WebIDE fourni dans Gitlab, modifier le fichier `README.md` sur le branche de l'issue et commiter
    * ajoute dans le fichier un bloc de code *```mermaid* 
* Enfin, accepter la MR pour que le code soit mergé sur la branche `main`

```
✅ Le fichier README.md sur main est à jour avec le schéma mermaid
✅ L'issue a été automatiquement fermée lorsque le merge est terminé
```

Si l'issue n'a pas été fermée, c'est qu'il vous a manqué l'indication `Closes #1` dans la description de la MR.
Normalement, cela est positionné automatiquement si vous créez la MR depuis l'issue.

## Instanciation de la CI

**NB:** Gitlab fournit une aide pour initialiser ce type de fichier via `Set up CI/CD`.

Dans le cadre de ce workshop, nous allons créer le fichier *from scratch* pour bien comprendre les différents éléments 💪

### Mon premier job

Toujours dans le même repository:
* ajouter un fichier `.gitlab-ci.yml` en passant par le menu `CI/CD > Editor`
* au sein de ce fichier:
    * supprimer les jobs/stages existants
    * ajouter un [stage](https://docs.gitlab.com/ee/ci/yaml/#stages) `👋_hello`
    * ajouter un [job](https://docs.gitlab.com/ee/ci/jobs/#group-jobs-in-a-pipeline) `🌍_hello-world` dans le stage `👋_hello`
        * ce job doit affiché `👋 Hello Devoxx 2023 🍺` dans les logs
    * commiter le fichier sur la branche `main` directement
* aller dans le menu `CI/CD > Pipelines` et constater qu'un [pipeline](https://docs.gitlab.com/ee/ci/pipelines/) s'est déclenché
    * les logs sont accessibles en cliquant sur le nom du job dans le pipeline

```
✅ Le pipeline doit s'exécuter avec succès 🎉
```

### Utilisation des variables

Pour certaines données sensibles, ou dynamiques en fonction de la branche sur laquelle le pipeline va s'exécuter, il est nécessaire de variabiliser certaines données.

Ensuite, toujours dans le fichier `gitlab-ci.yml` depuis le menu `CI/CD > Editor`:
* remplacer `Devoxx 2023 🍺` par une [variable](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project) définie au niveau du projet via le menu `Settings > CI/CD > Variables` et modifier sa valeur avec une autre conférence par exemple `Devoxx 2024 🚀`
* modifier également le message pour qu'il affiche le nom du projet et la branche sur laquelle s'exécute le pipeline grâce aux [variables pré-définies](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
* commiter le fichier sur la branche `main` directement
* retourner dans le menu `CI/CD > Pipelines` et constater qu'un [pipeline](https://docs.gitlab.com/ee/ci/pipelines/) s'est de nouveau déclenché

```
✅ Le pipeline doit s'exécuter avec succès 🎉
✅ Dans le log du job doit être affiché :

     👋 Hello Devoxx 2024 🚀 from xxx_project on branch main
```

### Plusieurs stages

Il est possible au sein de la CI:
* d'avoir plusieurs stages
* de définir l'image docker utilisée pour exécuter le job. Cela peut être une image officielle du hub docker ou une image custom

Dans le fichier `.gitlab-ci.yml`:
* ajouter un nouveau stage `🎨_ascii-art`
* ajouter 3 jobs en lien avec ce stage:
    * `🐮_meuh` : en utilisant une image `ubuntu`, installer l'outil `cowsay` avant l'exécution du script du job ([💡 astuce](https://docs.gitlab.com/ee/ci/yaml/#before_script)) et afficher une vache dans le log du job
        * 💡 On installe cowsay comme un package linux classique : `apt-get update && apt-get install -y cowsay`
        * 💡 Cowsay s'intalle dans `/usr/games`, il faut donc utiliser `/usr/games/cowsay` dans le job
        * 🐒 ou tout simplement en utilisant ce [snippet](https://gitlab.com/gitlabuniversity/workshop-snippets/-/snippets/2519494)
    * `🐴_my-little-poney` : en utilisant l'image `mpepping/ponysay` de l'outil [ponysay](https://github.com/erkin/ponysay), afficher un poney dans le log
        * 🐒 ou tout simplement en utilisant ce [snippet](https://gitlab.com/gitlabuniversity/workshop-snippets/-/snippets/2519495)
    * `🦖_jurassic-park` :  en utilisant l'image `python`, installer [dinosay](https://github.com/MatteoGuadrini/dinosay) et afficher un dinosaure dans le log
        * pour installer un package en python : `pip install xxx`
        * 🐒 ou tout simplement en utilisant ce [snippet](https://gitlab.com/gitlabuniversity/workshop-snippets/-/snippets/2519496)

```
✅ Le pipeline et les 4 jobs doivent s'exécuter avec succès 🎉
```

On peut voir que dans cet exemple, les jobs du stage `🎨_ascii-art` attendent que le stage `👋_hello` soit terminé, puis ils s'exécutent en parallèle sans ordre précis.

Dans cette étape, nous allons forcer l'ordre des jobs:
* `🐮_meuh` ne doit pas attendre que le stage `👋_hello` s'exécute
* `🐴_my-little-poney` doit attendre que `🌍_hello-world` soit terminé
* `🦖_jurassic-park` doit attendre que `🐮_meuh` soit terminé

👉 [astuce](https://docs.gitlab.com/ee/ci/yaml/#needs)

On peut voir simplement les liens entre les jobs soit via l'option `Jobs dependencies` soit via l'onglet `Needs` dans la page d'exécution du pipeline.

```
✅ Le pipeline et les 4 jobs doivent s'exécuter avec succès et dans le bon ordre 🎉
```

### Gestion des règles

Toujours dans le même projet:
* créer une issue `📏 Set up rules`
* créer une MR et une branche pour cette issue
* sur la branche nouvellement créée, modifier le fichier `.gitlab-ci.yml` de manière à ce que le job `🐴_my-little-poney` ne s'exécute que sur la branche `main`
    * [💡 astuce](https://docs.gitlab.com/ee/ci/yaml/#rules)
* commiter sur la branche

```
✅ Le pipeline s'exécute correctement 🎉 et le job 🐴_my-little-poney ne s'exécute pas ✋
```
* via la MR, merger la branche sur `main`
* vérifier que le pipeline s'exécute

```
✅ Le pipeline s'exécute correctement 🎉 et le job 🐴_my-little-poney s'exécute sur la branche main 🤡
```

## Utilisation de la fonctionnalité Pages

Gitlab propose la fonctionnalité [Pages](https://docs.gitlab.com/ee/user/project/pages/) pour facilement exposer un site statique à partir de sources dans un repository.

### Ma première Pages

* Dans le meme projet, via le *WebIDE*
    * créer un répertoire `public` contenant un fichier `index.html` avec un petit bout de code HTML simple ([snippet](https://gitlab.com/gitlabuniversity/workshop-snippets/-/snippets/2519497))
    * modifier fichier `.gitlab-ci.yml` à la racine du projet
    * ajouter un stage `deploy` et un job `pages` pour ce stage
        * ceux sont 2 mots clés protégés en Gitlab CI
        * faire du répertoire `public` un [artifact](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html) du job
    * commiter sur `main`
    * aller voir le pipeline qui s'exécute

```
✅ Le pipeline doit s'exécuter avec succès 🎉
```

**NB:** *un job a été automatiquement ajouté `pages:deploy`, c'est celui-ci qui déploie le site statique*

Une fois le pipeline exécuté correctement:
* aller dans le menu `Settings > Pages` (ou `Deployments > Pages`)
* trouver l'url de votre site

```
✅ Le site est déployé et accessible 👍
```

### Mon premier environnement

Pour simplifier l'accès au site, on peut également le lier à un [environnement](https://docs.gitlab.com/ee/ci/environments/)

Dans le fichier `.gitlab-ci.yml` en passant soit par le *WebIDE* soit par le menu `CICD > Editor` :
* ajouter le paramétrage nécessaire dans le job `pages` pour qu'un environnement soit créé avec le nom de la branche
* commiter
* une fois le pipeline exécuté avec succès, aller dans le menu `Deployments > Environments`

```
✅ Le site est déployé et accessible depuis le menu Environments avec le nom `main`
```

L'utilisation des environnements pour les pages reste limitée. Plus globalement, cela permet de déployer une application et la lier à un environnement et donc une *version* du code. Cela amène de la tracabilité sur quelle version a été déployée et quand.

**NB:** Ce n'est pas GitLab qui héberge l'application déployé. GitLab fait le lien entre un environnement (une URL) et la version du code source associée. 

### Release du projet

On peut classiquement tagger notre branche `main` pour fixer une version. GitLab propose en complément la notion de [Release](https://docs.gitlab.com/ee/user/project/releases/) permettant d'avoir un package comprenant cette version.

Depuis la version [15.7](https://about.gitlab.com/releases/2022/12/22/gitlab-15-7-released/), une nouvelle CLI est disponbible : [glab](https://gitlab.com/gitlab-org/cli). Celle-ci permet d'intéragir facilement avec GitLab et entre autres de créer une release.

* Dans le `.gitlab-ci.yml`, ajouter un job **📦_release-with-glab** dans le stage **release** pour créer une release à partir d'un tag
    * pour que glab fonctionne il faut d'abord [s'authentifier](https://gitlab.com/gitlab-org/cli#authentication) en utilisant un [access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) que l'on peut générer depuis le menu `Preferences > Access Token`, accessible en cliquant sur son avatar en haut à droite. Il faut que le token ait les droits `api` & `write_repository`
    * Une fois authentifié, on peut alors générer sa release
    * 💡 astuce : [snippet partiel](https://gitlab.com/gitlabuniversity/workshop-snippets/-/snippets/2519498) (il faut remplacer les `****`)
    * 🔑 Mettre un token en dur dans le fichier `.gitlab-ci.yml` est une mauvaise pratique de sécurité. Mettre sa valeur dans une valeur de CI/CD
* Il est surement préférable que le job de release ne s'exécute que lorsque l'on est sur un pipeline lié à un tag... Ajouter une `rules` dans ce sens.
* Depuis le menu `Repository > Tags`, créer un tag `v1.0`
* Générer une release avec comme titre `🗼 Devoxx Release <nom_tag>`

```
✅ La release apparait dans Deployments -> Releases avec le titre 🗼 Devoxx Release <nom_tag>
✅ Le job de release ne s'exécute que sur les pipelines liés à un tag
✅ Les pipelines de main & des tags s'exécutent correctement
```

### Badges décoratifs

On peut associer décorer le projet avec des [badges](https://docs.gitlab.com/ee/user/project/badges.html) visibles sur la page d'accueil du projet. Cela est pratique pour avoir des informations sur le statut du pipeline, la couverture de tests, la licence, ...

Depuis le menu `Settings > General > Badges`
* Ajouter le badge de statut du pipeline

```
✅ Le badge du statut du pipeline apparait sur la page d'accueil
```